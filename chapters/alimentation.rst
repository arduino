.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

.. default-role:: literal
.. role:: smallcaps
.. role:: index

USB
===

L'|usb| fourni une tension de 5 |V| avec une intensité maximale de 100 |mA| sur
les bornes d'alimentation. Il est donc possible d'alimenter une carte Arduino
avec un câble |usb| (c'est d'ailleurs ainsi que l'on procède lorsque l'on
programme la carte).

Source externe
==============

.. sidebar:: Régulateur LM1117

  .. image:: content/LM1117.jpg
    :align: center
    :width: 75%

La carte Arduino possède un régulateur de tension intégré capable de produire
une tension de  5 |V|. Il est donc possible d'alimenter la carte avec une
tension supérieure pour alimenter le circuit.

.. note::

  La régulation de la tension est réalisée en convertissant la puissance
  superflue en chaleur. Si la tension est trop importante, le composant va trop
  chauffer et la carte va s'abîmer.

  De même, le régulateur consomme également du courant en fonctionnement,
  donner une tension de 5 |V| sera trop juste pour alimenter le circuit.

  Aussi, la tension recommandée pour un bon fonctionnement est comprise entre 7
  à 12 |V|.

.. sidebar:: Attention au sens !

  En inversant le sens des fils entre la masse et l'alimentation, on applique
  l'opposé de la tension attendue, ce qui ne plaira pas beaucoup au circuit !
  [#]_

.. [#] https://www.rugged-circuits.com/10-ways-to-destroy-an-arduino

Il existe deux points de connexions sur la carte permettant d'utiliser le
régulateur de tension : la prise jack, ainsi que la borne `Vin`.

Alimentation via prise jack
---------------------------

Il s'agit de la manière la plus simple pour alimenter le circuit, puisque la
carte Arduino possède une prise Jack. En respectant les limites des tensions
indiquées ci-dessus, il ne reste qu'à brancher la fiche sur la carte.

Dans le cas d'un transformateur externe, il faut veiller à ce que symbole suivant

.. image:: content/polarity.pdf
  :width: 25%
  :align: center

soit présent sur la carte : c'est à dire que le fil `+` soit connecée au centre
de la borne d'alimentation.

Alimentation directe sur la carte
---------------------------------

La borne `Vin` permet de connecter directement une alimentation à la carte :
elle est également reliée au régulateur de tension et supporte donc une tension
supérieure jusque 12 |V|.

Là encore, il faut veiller à connecter les deux fils sur les bonnes bornes : la
carte Arduino n'offre aucune protection en cas d'erreur…

Alimentation externe 5 |V|
==========================

.. note::

  Cette solution peut être utilisée si la tension d'entrée est supérieure à
  12 |V| : l'utilisation d'un régulateur de tension externe tel que le `LM7805`
  permettra de prendre en entrée une tension jusqu'à 20 |V|.

Si la tension externe est déjà stabilisée à 5 |V|, il n'est pas possible de se
connecter sur la broche `Vin` de la carte, puisqu'il faut au minimum du 7 |V|
pour avoir une tension de fonctionnement correcte.

Alimentation par usb
--------------------

.. sidebar:: Cable

  .. image :: content/USB-2-0-B-MALE-TO-5.jpg
    :width: 100%

La solution la plus sûre dans ce cas est de brancher cette alimentation sur
l'entrée |usb| de la carte. On gagne ainsi en sécurité, au détriment d'un petit
peu de cablage supplémentaire.

Alimentation par la broche 5V
-----------------------------

Autre solution envisageable, brancher l'alimentation directement sur la broche
`5V` de la carte Arduino : cette borne est reliée au circuit 5 |V| de la carte,
et n'est pas uniquement une borne de sortie : elle relie l'ensemble des
composants qui ont besoin d'être alimentés avec cette tension.

.. admonition:: Attention
  :class: warning

  Alimenter la carte par la prise `5V` de la carte n'est pas recommandé :

  - Étant donné que l'on passe outre le régulateur de tension, il y a un risque
    important de griller la carte si l'on applique une tension trop importante.
  - Il interdit d'utiliser en même temps la prise |usb|
  - Il interdit d'utiliser en même temps la prise `Vin` sous risque de griller
    la carte

En effet, cette prise est directement reliée au circuit 5 |V| et outrepasse
**toutes** les protections de la carte, qui n'est pas prévue pour être
alimentée ainsi. Il faut réserver cet usage a des cas particuliers (par exemple
alimenter deux cartes Arduino ensemble)

Tableau récapitulatif
=====================

Les différentes tensions admissibles :

================  =============== =============== ==============
Entrée            Tension min.    Tension max.    Intensité max.
================  =============== =============== ==============
Port USB          4,5 |V|         5,5 |V|
Broche `5V`       4,5 |V|         5,5 |V|
Prise jack        7 |V|           12 |V|
Broche `Vin`      6,6 |V|         12 |V|
================  =============== =============== ==============

