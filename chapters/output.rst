.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Premier programme
=================

Faire clignoter une |led| est l'une des premières étapes avec Arduino. C'est
aussi une introduction pour comprendre le fonctionnement de la carte.

Ce premier programme ne nécessite pas de branchement : une |led| est présente
sur la carte et devrait s'allumer automatiquement en lançant le programme.

Décomposition du programme
--------------------------

L'initialisation
~~~~~~~~~~~~~~~~

L'initialisation s'exécute seulement au démarrage du programme sur la carte
(et sera de nouveau exécutée si l'on appuie sur le bouton `reset`).

.. code-block:: arduino
  :number-lines: 9

  // the setup routine runs once when you press reset:
  void setup() {
    // initialize the digital pin as an output.
    pinMode(13, OUTPUT);
  }

Une seule instruction est lancée dans cet extrait : :index:`pinMode`
`pinMode` [#]_. Celle-ci permet d'indiquer au microcontrolleur si la broche
doit être configurée pour envoyer du courant, ou en recevoir. Puisque nous
souhaitons ici allumer une |led|, nous lui passons le paramètre `OUTPUT` en
argument.

.. [#] https://www.arduino.cc/en/Reference/pinMode

L'exécution
~~~~~~~~~~~

Voici le cœur du programme, ces instructions seront exécutées en boucles,
indéfiniment, tant que la carte sera sous tension.

.. code-block::  arduino
  :number-lines:  15

  // the loop routine runs over and over again forever:
  void loop() {
    // turn the LED on (HIGH is the voltage level)
    digitalWrite(13, HIGH);
    delay(1000);               // wait for a second
    digitalWrite(13, LOW);
    // turn the LED off by making the voltage LOW
    delay(1000);               // wait for a second
  }

Ce bloc est composé de quatre instructions, qui appellent deux fonctions
différentes.  :index:`digitalWrite` `digitalWrite` [#]_ permet d'envoyer ou de
couper le courant sur une broche donnée. Pour allumer la |led|, nous commençons
donc en donnant `HIGH` en paramètre.

.. [#] https://www.arduino.cc/en/Reference/digitalWrite

Vient ensuite une instruction permettant de faire une pause dans le programme :
:index:`delay` `delay` [#]_. Cette fonction prend en paramètre la durée de la
pause (en millisecondes, `1000` vaut donc une seconde).

.. [#] https://www.arduino.cc/en/Reference/delay

Afin de faire clignoter la |led|, il est nécessaire de l'éteindre ensuite.
C'est pourquoi nous retrouvons l'instruction `digitalWrite`, mais cette fois
avec la valeur `LOW`, suivi d'une nouvelle pause d'une seconde.

.. sidebar:: digital

  digital signifie *numérique*. Le terme indique une valeur que l'on peut
  exprimer en nombre entier (1, 2, 3…).

Le programme complet
--------------------

.. sidebar:: Chargement

  Ce programme est disponible dans l'environnement Arduino dans les exemples :
  Basic / Blink

.. code-block:: arduino

  /*
    Blink
    Turns on an LED on for one second, then off for one
    second, repeatedly.

    This example code is in the public domain.
   */

  // the setup routine runs once when you press reset:
  void setup() {
    // initialize the digital pin as an output.
    pinMode(13, OUTPUT);
  }

  // the loop routine runs over and over again forever:
  void loop() {
    // turn the LED on (HIGH is the voltage level)
    digitalWrite(13, HIGH);
    delay(1000);               // wait for a second
    digitalWrite(13, LOW);
    // turn the LED off by making the voltage LOW
    delay(1000);               // wait for a second
  }

Utiliser une led externe
========================

.. sidebar:: Une led

  .. image:: content/5mm_Red_LED.jpg
    :align: center
    :width: 100%

Suite à ce premier exemple, nous allons maintenant faire évoluer notre
programme pour l'adapter à une |led| que nous allons brancher à la carte.
Puisque nous intégrons un composant externe, nous allons devoir veiller à ce
que celui-ci soit compatible avec l'alimentation de la carte Arduino : certains
composants nécessitent une tension plus importante (par exemple un moteur),
d'autre des tensions plus faibles (au risque de faire griller le composant).

Le tableau suivant indique la valeur minimale et maximale supportée par
différents types de |led|, ainsi que l'intensité du courant pour la tension
minimale et maximale.

======= =========== ============== ================
Taille  Couleur     Tension (`V`)  Intensité (`mA`)
======= =========== ============== ================
3mm     rouge       `1.8`-`2.5`    `5`-`18`
3mm     vert        `2.1`-`2.6`    `5`-`17.5`
3mm     jaune       `1.8`-`2.0`    `5`-`17.5`
5mm     rouge       `1.9`-`2.3`    `5`-`18`
5mm     vert        `1.9`-`2.5`    `4`-`14`
5mm     jaune       `1.9`-`2.5`    `5`-`15`
======= =========== ============== ================

.. sidebar:: Polarisation

  la |led| ne laisse passer le courant que dans un seul sens. C'est pourquoi
  les deux broches ne sont pas de la même longueur. La patte la plus courte
  doit être branchée sur le pôle `-`.

.. admonition:: Attention
  :class: warning

  La tension de sortie de la carte est de 5 |V|. Or, en lisant le tableau, on
  voit bien que les |led|\ s ne supporteront pas cette tension à leur borne, il
  est donc nécessaire de mettre en place un système pour réduire la tension
  dans les limites de ce qui est acceptable par la |led|.

Dans notre situation, la tension délivrée par la carte Arduino et le courant
qui traversera la |led| sont stables — la |led| ne modifie pas son comportement
pendant son fonctionnement. Nous pouvons utiliser le schéma le plus simple pour
réduire la tension : *la résistance chutrice*.

.. figure:: content/cligo_led.pdf
  :width: 100%

  Arduino, LED et une résistance

Calcul de la résistance
-----------------------

La résistance à placer se calcule de la manière suivante :

.. math::

    R   =   \frac{(U-Ul)}{I}

.. sidebar:: Valeurs

  :R: Résistance
  :U: Tension de l'alimentation
  :Ul: Tension de la |led|
  :I: Intensité de la |led|

Dans notre exemple, en prenant l'intensité maximale supportée par la |LED| nous
obtenons le calcul suivant :

.. math::

    R   &=   \frac{(5-2.5)}{0.018} \\
        &=  139\ \Omega

.. sidebar:: Déjà des calculs ?

  Ce petit calcul peut sembler théorique, toutefois nous en aurons besoin par
  la suite, quand il sera nécessaire d'alimenter des composants avec des
  tensions différentes que l'alimentation. (Nous aborderons cela plus loin avec
  les notions de *Pont diviseur*, ainsi que les *régulateurs de tension*).

La résistance choisie doit donc avoir une valeur supérieure. En consultant la
série E12 [#]_, on peut choisir la valeur la plus proche (par exemple 220
:math:`\Omega`).

.. [#] Série de douze valeurs utilisées dans les composants électroniques :
       https://fr.wikipedia.org/wiki/CEI_60063

Vérification de l'intensité
---------------------------

On sait que : :math:`I   = \frac{U}{R}` soit ici :

.. math::

    I   &= \frac{U-Ul}{R} \\
        &= \frac{5-2.5}{220} \\
        &= 11.36\ mA

Ce qui est suffisant pour alimenter notre composant.


Mise à jour du programme
------------------------

Nous pouvons maintenant mettre à jour notre programme. Il s'agit toujours du
même code que celui présenté plus haut avec toutefois une légère différence :
au lieu d'envoyer du courant sur la broche `13`, nous l'envoyons désormais sur
la broche `9`. Il est donc nécessaire de faire quelques modifications au lignes
`12`, `18` et `20`.

Faire varier la luminosité
==========================

Il existe 6 ports sur la carte Arduino permettant de faire varier la tension de
sortie : `3`, `5`, `6`, `9`, `10` et `11`. Les autres port permettent seulement
d'envoyer la tension `HIGH` ou `LOW`, c'est-à-dire 5 |V| ou 0 |V|.

Il est possible d'utiliser la fonction :index:`analogWrite` `analogWrite` [#]_
sur chacun de ses ports, en envoyant une valeur comprise entre `0` et `255`.

.. [#] https://www.arduino.cc/en/Reference/AnalogWrite

.. raw:: latex


  \begin{center}
  \begin{tikzpicture}[scale=2.6]

  \foreach \x in {0,0.5,...,5} {
    \pgfmathtruncatemacro\result{\x * 255 / 5}
    \draw (\x,-4pt) -- (\x,4pt)
        node [below,yshift=-20] {\pgfmathprintnumber{\result}}
        node [above] {$\x$V};
  }

  \draw (0,0) -- (5,0);

  \end{tikzpicture}
  \end{center}

.. sidebar:: `PWM`

  La variation de la tension est réalisée via la *modulation de largeur
  d'impulsions* [#]_ qui permet de faire varier la tension en faisant varier de
  manière très brève la durée pendnant laquelle le courant est émis.

.. [#] https://fr.wikipedia.org/wiki/Modulation_de_largeur_d'impulsion

.. note::

  Les valeurs présentées ici sont des tensions *moyennes*. En envoyant la
  valeur `127`, la tension sur la broche sera de 5 |V|, mais pendant une durée
  très courte :

  .. raw:: latex

    \begin{tikzpicture}
      \draw[help lines] (0,0) grid (7,1);
      \draw[thick] (0,0) node[anchor=base east] {0V} -- ++(0,0)
        \foreach \x in {1,...,7}{
        -- +(0,1) -- +(0.5,1) -- +(0.5,0) -- ++(1,0)
        } -- +(0,.5);
      \draw[thick,anchor=north east] (0,1) node {5V};
    \end{tikzpicture}

  Avec la valeur `192` la tension reste à 5 |V| pendant une durée plus longue :

  .. raw:: latex

    \begin{tikzpicture}
      \draw[help lines] (0,0) grid (7,1);
      \draw[thick] (0,0) node[anchor=south east] {0V} -- ++(0,0)
        \foreach \x in {1,...,7}{
        -- +(0,1) -- +(0.75,1) -- +(0.75,0) -- ++(1,0)
        } -- +(0,.5);
      \draw[thick,anchor=north east] (0,1) node {5V};
    \end{tikzpicture}

  Dans tous les cas, la tension à la sortie de la borne sera de 5 |V|. Il est
  toutefois possible de transformer ce signal en une vraie tension linéaire, à
  l'aide d'un condensateur supplémentaire [#]_.

.. [#] https://provideyourown.com/2011/analogwrite-convert-pwm-to-voltage/

.. admonition:: Mesure de la tension
  :class: exercice

  Avec un multimètre, mesurer la tension en sortie de la borne en fonction de
  diverses valeurs.

Programme
---------

.. sidebar:: Ressource limitée !

  Cette capacité à contrôler la tension de sortie est très intéressante (par
  exemple pour demander à un moteur de tourner plus ou moins vite), mais est
  limitée à 6 bornes seulement. Il vaut mieux les réserver quand nous avons
  besoin d'une sortie analogique.

.. code-block:: arduino

  // Fade

  // This example shows how to fade an LED on pin 9
  // using the analogWrite() function.

  // This example code is in the public domain.

  int led = 9;        // the pin that the LED is attached to
  int brightness = 0; // how bright the LED is
  int fadeAmount = 5; // how many points to fade the LED by

  // the setup routine runs once when you press reset:
  void setup() {
    // declare pin 9 to be an output:
    pinMode(led, OUTPUT);
  }

  // the loop routine runs over and over again forever:
  void loop() {
    // set the brightness of pin 9:
    analogWrite(led, brightness);

    // change the brightness for next time through the loop:
    brightness = brightness + fadeAmount;

    // reverse the direction of the fading
    // at the ends of the fade:
    if (brightness == 0 || brightness == 255) {
      fadeAmount = -fadeAmount ;
    }
    // wait for 30 milliseconds to see the dimming effect
    delay(30);
  }


Générer du son
==============

.. sidebar:: Un piezzo buzzer

  .. image:: content/piezzo_buzzer.jpg
    :align: center
    :width: 100%

Avec un piezzo buzzer il est possible d'ajouter une sortie sonore au programme.
Ceux que l'on trouve dans le commerce supportent une tension allant de 3 à
24 |V|, ce qui permet de brancher le buzzer directement en sortie de la carte.

Le composant permet de produire du son en fonction de la fréquence du signal de
sortie.

Il est possible d'utiliser la fonction `digitalWrite` sur une borne pour
produire un beep, mais il est plus intéressant de pouvoir contrôler la
fréquence de sortie. Nous utiliserons pour cela la fonction :index:`tone`
`tone` [#]_. (Cette instruction peut être utilisée sur une sortie numérique, il
n'est donc pas nécessaire de réserver une sortie analogique pour le buzzer.)

.. [#] https://www.arduino.cc/en/Reference/Tone

.. sidebar:: Qualité du son

  La fonction `tone` ne sait produire qu'un signal carré, ce qui signifie que
  le son n'aura jamais les harmoniques d'un fichier :smallcaps:`mp3` ou
  :smallcaps:`wav`.

.. note::

  La fonction `tone` n'est pas disponible sur toutes les cartes Arduino. Si
  vous utilisez une carte *Gemma* ou *Due* (ou un autre microcontrolleur), la
  fonction ne sera pas disponible.

  Dans ce cas, il est possible d'écrire soit même la fonction en utilisant
  les instructions `delayMicroseconds` et `digitalWrite`. Dans ce cas la
  fréquence des sons ne sera pas garantie, mais le but n'est pas de faire de la
  carte Arduino un outil `hi-fi` :

  .. code-block:: arduino

    void tone(int targetPin, long frequency, long length) {
      // calculate the delay value between transitions
      // 1 second's worth of microseconds, divided by the
      // frequency, then split in half since there are two
      // phases to each cycle
      long delayValue = 1000000/frequency/2;
      // calculate the number of cycles for proper timing
      // multiply frequency, which is really cycles per
      // second, by the number of seconds to get the
      // total number of cycles to produce
      long numCycles = frequency * length/ 1000;
      for (long i=0; i < numCycles; i++) {
        // write the buzzer pin high to push out the diaphram
        digitalWrite(targetPin,HIGH);
        // wait for the calculated delay value
        delayMicroseconds(delayValue);
        // write the buzzer pin low to pull back the diaphram
        digitalWrite(targetPin,LOW);
        // wait again or the calculated delay value
        delayMicroseconds(delayValue);
      }
    }

Par exemple, en connectant le buzzer sur la broche `8`, l'instruction suivante
permettra de jouer un  *la* pendant une seconde :

.. code-block:: arduino

  tone(8, 440, 1000);

En faisant varier la fréquence ainsi que la durée de chaque note, nous pourrons
ainsi jouer des mélodies (simples). Il est par contre nécessaire de connaître
la fréquence de la note que l'on souhaite jouer. (La table peut être obtenue
facilement sur internet [#]_).

.. [#] https://fr.wikipedia.org/wiki/Note_de_musique

.. sidebar:: Codification des notes

  En anglais, chaque note est codée par une lettre de A à G : C pour Do, D pour
  Ré, F pour Mi, etc.

Plutôt que d'écrire directement les fréquence dans le programme, il est plus
simple de coder les notes, et laisser l'application jouer les notes
correspondantes ; la notation anglaise est souvent utilisée. En associant
chaque fréquence à une valeur dans le programme, il devient plus facile
d'écrire la mélodie qui peut être codée en écrivant directement les notes de
musiques !

.. admonition:: Exercice
  :class: exercice

  Écrire une petite mélodie !

Contrôler un moteur
===================

Avec ces instructions, il est temps de passer aux choses sérieuses. En
contrôlant les sorties à l'aide de l'instruction `digitalWrite`, nous allons
pouvoir donner du mouvement au robot en lui donant des roues. Mettre en route
un moteur n'est pas difficile, mais nécessite de prendre certaines précautions
avant de connecter notre moteur.

.. admonition:: Attention
  :class: warning

  Ne jamais brancher un moteur directement à la carte Arduino. D'une part parce
  que la tension nécessaire pour activer le moteur risque d'être trop
  importante pour la carte (dans le meilleur des cas, le moteur ne tournera
  pas, dans le pire des cas la carte grillera), mais aussi parce qu'un moteur
  fonctionne de la même manière qu'une dynamo : il produit du courant quand il
  tourne. Ce courant risque de remonter dans la carte et générer un
  court-circuit.

Pour cette raison, nous utiliserons un contrôleur externe, destiné à nous
éviter de nombreuses complications : le L293D [#]_. Cette puce va nous permettre :

.. [#] http://www.ti.com/lit/ds/symlink/l293d.pdf [#]_

.. [#]  Prenez le temps de lire les spécification des composants. Elles sont en
        anglais, mais donnent toutes les informations nécessaire pour utiliser
        le composant au mieux : les caractéristiques générale, mais également
        les cas d'utilisation, les valeurs maximales supportées, les conseils
        de branchement etc.

.. sidebar:: Une puce L293D

  .. image:: content/L293D_Motor_Driver.jpg
    :align: center
    :width: 100%

- de protéger la carte Arduino en empêchant les surtensions : il intègre des
  diodes de protections et des contrôleurs thermiques pour protéger le circuit.
- de limiter le cablage et les composants à utiliser : le composant supporte
  des tensions de fonctionnement allant de 4,5 à 36 |V|, et permettra donc de
  délivrer des tensions beaucoup plus puissantes que ce que peut fournir la
  carte Arduino.

.. note::

  Il est parfaitement possible de contrôler un moteur directement à partir de
  la carte Arduino sans utiler ce type de composant, mais nous compliquons
  alors le schéma de cablage inutilement. La documentation technique du
  composant nous montre comment réaliser le cablage équivalent avec des diodes
  et des transitors (un double pont en H).

En fait, la puce ne permet pas simplement de contrôler un moteur, mais quatre
! (ou alors deux moteurs pouvant tourner dans les deux sens). Cela correspond à
notre robot qui aura deux roues motrices : une seule puce et quelques fils
pourront permettre de faire rouler notre robot et lui faire réaliser quelques
manœuvres.

.. figure:: content/l293d.pdf
  :width: 50%

  L293D

  :EN:      Puissance du moteur : permet de contrôler la vitesse de rotation du
            moteur en fonction de la tension appliquée à la borne. Cette borne
            peut être reliée à une borne analogique de la carte Arduino.
  :A:       Activation du moteur : permet d'activer le moteur connecté. Ces
            bornes sont à relier aux sorties digitales de la carte.
  :Y:       Sortie à relier aux bornes des moteurs.
  :VCC2:    Alimentation des moteurs : cette borne est à relier à
            l'alimentation générale (36 |V| max.)
  :VCC1:    Alimentation du composant (5 |V|) : cette borne est à relier à
            l'alimentation stabilisée de la carte Arduino.
  :ground:  Ces bornes sont à relier à la masse

Montage général
---------------

Il y a trois types de bornes sur le composant : celles destinées à être
connectées au moteur (`Y`), celles destinées à la carte Arduino (`EN` et `A`),
et celles qui qui servent à l'alimentation (`VCC`). Comme il est possible de
contrôler plusieurs moteurs, les bornes sont numérotées en fonction du moteur
contrôlé.

Dans le cas d'un moteur devant tourner dans les deux sens, les bornes `Y`
doivent être connectées aux deux pattes du moteur : en envoyant du courant sur
l'une des deux bornes `A`, il devient possible de faire tourner le moteur dans
un sens ou un autre (le tableau présente l'exemple d'un moteur connecté aux
bornes `1` et `2`, un deuxième moteur pourra être contrôlé de la même manière
en le branchant sur les bornes `3` et `4`) :

.. sidebar:: frein électromagnétique

  Le moteur génère de l'électricité en tournant (comme une dynamo). En
  alimentant le moteur avec l'électricité qu'il produit lui-même, on peut ainsi
  freiner le moteur avec sa propre énergie !

=========== =========== =============================================
Tension 1A  Tension 2A  Résultat
=========== =========== =============================================
`LOW`       `LOW`       Arrêt du moteur (frein électromagnétique)
`LOW`       `HIGH`      Faire tourner le moteur dans un sens
`HIGH`      `LOW`       Faire tourner le moteur en sens inverse
`HIGH`      `HIGH`      Arrêt du moteur (frein électromagnétique)
=========== =========== =============================================

Voir le schéma de branchement avec deux moteurs contrôlés depuis la carte
Arduino.

.. figure:: content/arduino_l293d.pdf
  :width: 100%

  L293D, Arduino et deux moteurs

  Les cables reliant le L293D à la masse, et l'alimentation des moteurs ne sont
  pas représentés.

  :Cable bleu:  Marche avant
  :Cable vert:  Marche arrière
  :Cable orange: Puissance du moteur

.. raw:: latex

  \pagebreak

Programme
---------

.. sidebar:: Valeurs

  Les valeurs ne sont pas choisies par hasard : elles correspondent aux
  paramètres des fonctions `digitalWrite` et `analogWrite` !

Ce programme n'est pas disponible dans l'environnement Arduino, cette fois nous
allons devoir l'écrire nous même. Afin de simplifier l'écriture, nous allons
définir une fonction `runMotor` qui prendra trois paramètres :

:motor:     Le numéro du moteur à activer, le 1er moteur déclaré aura la numéro `0`
:direction: La direction du moteur, elle peut prendre deux valeurs `HIGH` ou
            `LOW`
:speed:     La vitesse de rotation, cette valeur peut aller de `0` à `255`

.. code-block:: arduino

  void runMotor(int motor, int direction, int speed) {
    // …
  }


.. admonition:: Contrôle du moteur
  :class: exercice

  Avec la fonction `runMotor`, nous pouvons donner du mouvement au robot. En
  donnant des paramètres de sens et de vitesse différents à la roue gauche et
  droite le robot se déplacera différement. Donnez les paramètres de la
  fonction `runMotor` pour réaliser les actions suivantes :

  - Marche avant
  - Marche arrière
  - Faire tourner le robot sur lui-même
  - Faire un virage serré
  - Faire un virage large
  - Arrêt complet

.. ========================= ========================= ==========================
.. Moteur 1                  Moteur 2                  Effet
.. ========================= ========================= ==========================
.. `runMotor(0, HIGH, 255)`  `runMotor(1, HIGH, 255)`  Marche avant
.. `runMotor(0, LOW, 255)`   `runMotor(1, HIGH, 255)`  Tourner sur lui-même
.. `runMotor(0, HIGH, 255)`  `runMotor(1, LOW, 255)`   Tourner sur lui-même (sens
..                                                     inverse)
.. `runMotor(0, LOW, 255)`   `runMotor(1, LOW, 255)`   Marche arrière
.. `runMotor(0, LOW, 0)`     `runMotor(1, HIGH, 255)`  Virage serré
.. `runMotor(0, HIGH, 127)`  `runMotor(1, HIGH, 255)`  Virage large
.. `runMotor(0, LOW, 0)`     `runMotor(1, LOW, 0)`     Arrêt complet
.. ========================= ========================= ==========================

Connecter un écran Nokia 5110
=============================

.. sidebar:: L'écran LCD

  .. image:: content/s-l500.jpg
    :align: center
    :width: 100%

L'écran Nokia 5110 est un composant de base et peu honéreux (environ 2€)
permettant un affichage depuis la carte Arduino. Il permet un affichage sur une
grille de 84x48 pixels, soit 4 lignes de textes.

.. admonition:: Tension de fonctionnement
    :class: warning

    Le composant fonctionne sous 3,3 |V|, il n'est donc pas recommandé de le
    brancher directement sur la carte Arduino (même si certains le font [#]_).
    Dans le doute, commencez avec un correcteur de courant.

.. [#] https://circuitdigest.com/microcontroller-projects/nokia5110-graphical-lcd-arduino-interfacing

Level shifter 74HC4050N
-----------------------

Ce composant est *unidirectionnel* et *High to Low* : en alimentant le
composant en 3.3 |V|, nous avons la possibilité de générer une sortie à 3.3 |V|
à partir de la carte Arduino : le composant possède 6 entrées et 6 sorties : à
chaque fois que la tension sur une borne d'entrée est supérieure à la tension
d'alimentation, la sortie correspondante sera activée.

.. admonition:: Broches non connectées
    :class: note

    Deux bornes ne sont pas connectées sur le composants : les bornes `13` et
    `16`. Attention au moment du cablage !

.. sidebar:: Cablage

  Le cablage sur le composant `74HC4050N` importe peu, tant que chaque entrée
  est bien associée à la sortie correspondantes.

.. figure:: content/nokia_5110.pdf
  :width: 100%

  Arduino, Nokia 5110, 74HC4050N

  :Cable Rouge: +3.3 |V|
  :Cable Noir: Masse
  :Cable Vert: Broche 7 - 3 (SCE)
  :Cable Bleu: Broche 6 - 4 (RST)
  :Cable Orange: Broche 5 - 5 (D/C)
  :Cable Maron: Broches 11 - 6 (DN(MOSI))
  :Cable Cyan: Broche 13 - 7 (SeriaL CLock)
  :Cable Mauve: Broche 9 - 8 (LED)


Le programme [#]_

.. image:: content/nokia_5110_montage.jpg
    :width: 100%


.. [#] https://learn.sparkfun.com/tutorials/graphic-lcd-hookup-guide


.. default-role:: literal
.. role:: smallcaps
.. role:: index
.. |lcd| replace:: :smallcaps:`lcd`
.. |usb| replace:: :smallcaps:`usb`

