#!/usr/bin/env python
# -*- coding: utf-8 -*-

def get_value(r1, r2, vin):
    va = r2 * vin /(r2 + r1)
    print("Va: ", va)
    value = int((va * 1023) / vin)

    if value == 0:
        return 0, 0
    realr1 = r2 * ((1023. / value) - 1)
    return value, int(realr1)

if __name__ == "__main__":

    vin = float(input("vin? "))
    r2 = int(input("r2? "))
    while True:
        r1 = int(input("r1? "))
        value, realr1 = get_value(r1, r2, vin)
        i = vin / (r1 + r2)
        print(value, realr1, i)


