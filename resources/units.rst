.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

.. default-role:: literal

.. |V| unicode:: 0xA0 V
	:ltrim:

.. |cm| unicode:: 0xA0 cm
	:ltrim:

.. |mA| unicode:: 0xA0 mA
	:ltrim:

.. |Ohm| unicode:: 0xA0 :math:`\Omega`
  :ltrim:
