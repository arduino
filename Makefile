TEX=xelatex
SOURCE=chapters

SRC  =$(wildcard *.rst)
SVG  =$(wildcard content/*.svg)
DOT =$(wildcard *.dot)

PDFS = $(SRC:.rst=.pdf)
INCLUDED_TEX = $(DOT:.dot=.tex)

all: $(PDFS)

BIBS =$(wildcard *.bib)

CHAPTERS = $(sort $(shell find $(SOURCE) -name '*.rst' -print))
SOURCES = $(sort $(wildcard resources/*.tex))

null  :=
space := $(null) #
comma := ,
TEX_STYLE := $(subst $(space),$(comma),$(strip $(SOURCES)))
TEX_BIBS := $(subst $(space),$(comma),$(strip $(BIBS)))

PACK = $(shell sed -e 's/\#.*$$//' -e '/^$$/d' resources/modules)
PACKAGES := $(subst $(space),$(comma),$(strip $(PACK)))

tmp:
	mkdir tmp

# Image generation
content/%.pdf: content/%.svg
	inkscape -D -z --export-pdf=$@ $<

%.tex: %.dot
	dot2tex --autosize --figonly -o $@ $<

# Generate the latex file from rst
tmp/%.tex: %.rst $(INCLUDED_TEX) $(SOURCES) $(CHAPTERS)| tmp
	$$(command -v rst2latex rst2latex.py | head -n 1) \
		--no-section-subtitles \
		--no-section-numbering \
		--table-style=booktabs \
		--use-latex-citations \
		--use-latex-docinfo \
		--documentclass=article \
		--documentoption=12pt,A4,table \
		--syntax-highlight=short \
		--stylesheet=$(PACKAGES),$(TEX_STYLE) \
		$< $@
	sed -i -e 's/{c}{\\hfill ... continued on next page/{r}{... suite sur la page suivante/;s|^%$$||' $@

# Generate each pdf with latex
tmp/%.pdf: tmp/%.tex $(BIBS) | tmp
	# Delete the previous index file if present
	test -f "tmp/$*.ind" && rm "tmp/$*.ind" || true
	# First run
	$(TEX) -output-directory tmp "$<"
	#bibtex tmp/$*.aux && $(TEX) -output-directory tmp $< || echo "no bib"
	# Generate the index page
	if [ -s "tmp/$*.idx" ]; then \
		makeindex "tmp/$*"; \
		$(TEX) -output-directory tmp "$<"; \
	fi
	while grep 'Rerun to get ' "tmp/$*.log" ; do $(TEX) -output-directory tmp "$<" ; done

# Put the pdf in the right place
%.pdf: tmp/%.pdf | tmp
	cp $< $@

clean:
	rm -r tmp

%.css:	%.sass
	sassc $< $@

%.html: %.rst %.css
	$$(command -v rst2html rst2html.py | head -n 1) --stylesheet notes_arduino.css  $< $@

weasyprint.pdf: notes_arduino.html
	weasyprint notes_arduino.html -s notes_arduino.css weasyprint.pdf


