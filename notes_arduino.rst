.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

.. default-role:: literal
.. role:: smallcaps
.. role:: index
.. |led| replace:: :smallcaps:`led`

.. include:: resources/units.rst

.. sectnum::
  :depth: 2

****************
La carte Arduino
****************

.. image:: content/ARDUINO_UNO_DIP_01.png
    :width: 50%
    :align: center

.. contents::
  :depth: 2

..  A partir de maintenant, on insère un saut de page avant chaque nouvelle
    section, et l'on redéfini l'espace après le titre de la section.

.. raw:: latex

    \newcommand{\sectionbreak}{\clearpage}
    \titlespacing*{\section}{0pt}{1.1\baselineskip}{100pt}

===============================
Les sorties de la carte Arduino
===============================

.. include:: chapters/output.rst

===============================
Les entrées de la carte Arduino
===============================

.. include:: chapters/input.rst

=========================
L'alimentation du système
=========================

.. include:: chapters/alimentation.rst

==============
Le langage C++
==============

.. include:: chapters/c.rst

==============================
Créer sa propre plaque arduino
==============================

Il suffit d'un microcontrolleur pour faire fonctionner son programme arduino,
il n'est pas nécessaire d'acheter la carte déjà montée !

.. Avec un ATMege328
.. =================
..
.. Voir le tutoriel [#]_
..
.. .. [#] https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard
..
.. RBBB propose de monter soit même son arduino

Avec un AtTiny85
================

.. figure:: content/attiny_programatter.pdf
  :width: 100%

  Cablage du programmateur
